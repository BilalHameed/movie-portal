package com.example.movieportal.Constants

object Constants
{
    val ERROR = "fields can not be empty!"
    val RESULT_CODE = 100
    val GALLERY_REQUEST_CODE = 200
}