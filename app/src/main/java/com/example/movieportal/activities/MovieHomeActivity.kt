package com.example.movieportal.activities

import android.os.Bundle
import com.example.movieportal.R
import com.example.movieportal.base.BaseActivity
import kotlinx.android.synthetic.main.custom_toolbar.*

class MovieHomeActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_home)
        initialize()

    }

    // add new movies button click handling
    fun addMovies()
    {

            showToast("hello")
    }
    override fun initialize()
    {
        btn_add_movies_toolbar.setOnClickListener {

            addMovies()
        }
    }
}
