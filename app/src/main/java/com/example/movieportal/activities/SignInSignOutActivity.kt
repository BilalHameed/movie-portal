package com.example.movieportal.activities

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.WindowManager
import com.example.movieportal.Constants.Constants
import com.example.movieportal.base.BaseActivity
import com.example.movieportal.dialogs.ForgetPasswordDialog
import com.example.movieportal.dialogs.LoadingDialog
import com.example.movieportal.dialogs.VerifyEmailDialog
import com.example.movieportal.model.UserData
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_signin.*
import kotlinx.android.synthetic.main.forget_password_dialog.*
import kotlinx.android.synthetic.main.loading_dialog.*
import kotlinx.android.synthetic.main.verify_email_dialog.btn_cancel_verify_email_dialog
import kotlinx.android.synthetic.main.verify_email_dialog.btn_verify_email
import android.R.id
import android.app.Activity
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Typeface
import android.provider.MediaStore
import android.widget.TextView
import com.example.movieportal.R
import java.util.*


class SignInSignOutActivity : BaseActivity() {

    lateinit var userName:String
    lateinit var userPassword:String
    lateinit var userEmail:String
    var selectedImage:Bitmap? = null
    var photoUri:Uri? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        )
        setContentView(R.layout.activity_signin)
        initialize()

    }

    private fun nav_to_signUp()
    {
        container_sign_in.visibility  = View.GONE
        iv_profile_set.visibility = View.VISIBLE
        container_sign_up.visibility = View.VISIBLE
        container_sign_up.setAnimationOnAnyView(R.anim.right_to_left)
    }

    private fun nav_to_signIn()
    {
        container_sign_up.visibility  = View.GONE
        iv_profile_set.visibility = View.GONE
        container_sign_in.visibility = View.VISIBLE
        container_sign_in.setAnimationOnAnyView(R.anim.right_to_left)
    }

    private fun LoginUserAndCheckVerification()
    {

        val firebaseUser = auth.currentUser
        userEmail = username_sign_in.text.toString()
        userPassword = password_sign_in.text.toString()

        if (userEmail.isEmpty())
        {
            username_sign_in.requestFocus()
            username_sign_in.error = Constants.ERROR
        }
        else if (userPassword.isEmpty())
        {
            password_sign_in.requestFocus()
            password_sign_in.error = Constants.ERROR
        }
        else
        {
            dialog_loading.tv_loading.text = "Signing In"
            dialog_loading.show()
            auth.signInWithEmailAndPassword(userEmail,userPassword)
                .addOnCompleteListener {

                    if (it.isSuccessful)
                    {
                        if(firebaseUser!!.isEmailVerified)
                        {
                            dialog_loading.hide()
                            activityChangeWithFinish(MovieHomeActivity::class.java)
                        }
                        else
                        {
                            dialog_loading.hide()
                            dialog_verify.show()
                            dialog_verify.setCancelable(false)
                            username_sign_in.clear()
                            password_sign_in.clear()
                        }

                    }
                    else
                    {
                        dialog_loading.hide()
                        showToast("invalid email or password")
                    }
                }
        }


    }

    private fun RegisterAndVerifyEmailAddress()
    {

      userName = et_username_sign_up.text.toString()
      userPassword = et_password_sign_up.text.toString()
      userEmail = et_email_sign_up.text.toString()

        if (userName.isEmpty())
        {
            et_username_sign_up.requestFocus()
            et_username_sign_up.error = Constants.ERROR
        }
        else if (userPassword.isEmpty())
        {
            et_password_sign_up.requestFocus()
            et_password_sign_up.error = Constants.ERROR
        }
        else if (userEmail.isEmpty())
        {
            et_email_sign_up.requestFocus()
            et_email_sign_up.error = Constants.ERROR
        }

        else
        {
            dialog_loading.tv_loading.text = "Signing Up"
            dialog_loading.show()

            auth.createUserWithEmailAndPassword(userEmail,userPassword)
                .addOnCompleteListener { task ->

                    val firebaseUser = auth.currentUser
                    if (task.isSuccessful)
                    {
                        if (firebaseUser==null)return@addOnCompleteListener

                        if (!firebaseUser.isEmailVerified)
                        {
                            firebaseUser.sendEmailVerification()

                                .addOnCompleteListener(this) { task ->
                                    if (task.isSuccessful)
                                    {

                                        //saving user data in fire base database
                                        storeUserImage()

                                        dialog_loading.hide()
                                        showToast("Verification Email has been sent to ${firebaseUser.email}")
                                        dialog_verify.show()
                                        dialog_verify.setCancelable(false)
                                        et_username_sign_up.clear()
                                        et_email_sign_up.clear()
                                        et_password_sign_up.clear()
                                        selectedImage = null
                                        iv_app_logo.setImageResource(R.drawable.profile_image)

                                    }
                                    else
                                    {
                                        dialog_loading.hide()
                                        showToast("Failed to send verification email at ${firebaseUser.email}")
                                    }
                                }
                        }

                    }
                    else
                    {
                        dialog_loading.hide()
                        showToast("Invalid email account! Or No internet Connection")
                    }

                }
        }


    }

    private fun cancelDialog()
    {
        dialog_verify.dismiss()
    }

    private fun cancelDialogForgetPassword()
    {
        dialog_forget.dismiss()
    }

    private fun verifyEmailAddress()
    {
        val emailUrl = Uri.parse("https://mail.google.com/mail/u/0/#inbox")
        val email = Intent(Intent.ACTION_VIEW,emailUrl)
        startActivity(email)
    }

    private fun saveUserDetails(imageuri:String)
    {
        fbDatabase.getReference("UsersDetail")
            .child(auth.uid.toString())
            .setValue(UserData().also {
                it.userName = userName
                it.userId = auth.uid.toString()
                it.userEmail = userEmail
                it.userImage = imageuri
            })

    }

    private fun recoverPassword()
    {
        dialog_forget.show()
        dialog_forget.setCancelable(false)
        dialog_forget.btn_recover_password_dialog.setOnClickListener {

            if (dialog_forget.et_dialog_forget.text.isEmpty())
            {
                dialog_forget.et_dialog_forget.requestFocus()
                dialog_forget.et_dialog_forget.error = "please enter email!"
            }
            else
            {
                auth.sendPasswordResetEmail(dialog_forget.et_dialog_forget.text.toString())
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            dialog_forget.et_dialog_forget.clear()
                            showToast("Reset Link has been sent to your email")
                            dialog_forget.dismiss()
                        }
                        else
                        {
                            showToast("Failed to send reset email!")
                        }
                    }
            }

        }

    }

    private fun setUserProfilePicture()
    {

        val permission_gallary = ContextCompat.checkSelfPermission(this,android.Manifest.permission.READ_EXTERNAL_STORAGE)

        if (permission_gallary != PackageManager.PERMISSION_GRANTED)
        {
            requestUserForPermission()
        }
        else
        {

            val intent = Intent(Intent.ACTION_GET_CONTENT)
            intent.type = "image/*"
            startActivityForResult(intent,Constants.RESULT_CODE)
        }

    }

    private fun requestUserForPermission()
    {
        ActivityCompat.requestPermissions(this,
        arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE)
        ,Constants.GALLERY_REQUEST_CODE)
    }

    private fun storeUserImage()
    {

        val filename = UUID.randomUUID().toString()
        val storage = fbStorage.getReference("/UserProfileImages/$filename")
            storage.putFile(photoUri!!)
            .addOnSuccessListener {
                storage.downloadUrl.addOnSuccessListener {

                    saveUserDetails(it.toString())
                }

            }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode==Constants.RESULT_CODE)
        {
            if (resultCode== Activity.RESULT_OK)
            {
                if (data!=null)
                {
                    photoUri = data?.data
                    selectedImage = MediaStore.Images.Media.getBitmap(contentResolver,photoUri)
                    iv_app_logo.setImageBitmap(selectedImage)
                    val snackBar = Snackbar.make(findViewById(id.content),"Image Selected!",Snackbar.LENGTH_SHORT)
                    val view = snackBar.view
                    view.setBackgroundColor(Color.parseColor("#5F88EC"))
                    val view_text = snackBar.view.findViewById<TextView>(R.id.snackbar_text)
                    view_text.setTypeface(Typeface.MONOSPACE,Typeface.NORMAL)
                    snackBar.show()
                }
                else
                {
                    Snackbar.make(findViewById(id.content),"No Image Selected!",Snackbar.LENGTH_LONG)
                        .show()
                }

            }

        }
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode)
        {
            Constants.GALLERY_REQUEST_CODE->
            {
                if(grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED)
                {

                    val snack = Snackbar.make(findViewById(id.content),
                        "please provide permission to select picture!",
                        Snackbar.LENGTH_INDEFINITE)
                     val view_text = snack.view.findViewById<TextView>(R.id.snackbar_text)
                     val view_action = snack.view.findViewById<TextView>(R.id.snackbar_action)
                     val view = snack.view
                     view.setBackgroundColor(Color.parseColor("#5F88EC"))
                     view_text.setTextColor(Color.WHITE)
                     view_action.setTextColor(Color.WHITE)
                     view_action.setTypeface(Typeface.MONOSPACE,Typeface.BOLD)

                    snack.setAction("Cancel") {
                        snack.dismiss()
                    }.show()

                }
                else
                {

                    val intent = Intent(Intent.ACTION_GET_CONTENT)
                    intent.type = "image/*"
                    startActivityForResult(intent,Constants.RESULT_CODE)
                }
            }
        }
    }
    override fun initialize()
    {
        auth = FirebaseAuth.getInstance()
        fbDatabase = FirebaseDatabase.getInstance()
        fbStorage = FirebaseStorage.getInstance()

        dialog_loading = LoadingDialog(this)
        dialog_verify = VerifyEmailDialog(this)
        dialog_forget = ForgetPasswordDialog(this)

        nav_to_sign_up.setOnClickListener {

            nav_to_signUp()
            username_sign_in.clear()
            password_sign_in.clear()

            username_sign_in.error = null
            password_sign_in.error = null

        }

        et_nav_to_sign_in.setOnClickListener {

            nav_to_signIn()
            et_username_sign_up.clear()
            et_email_sign_up.clear()
            et_password_sign_up.clear()

            et_username_sign_up.error = null
            et_email_sign_up.error = null
            et_password_sign_up.error = null
        }

        btn_sign_up.setOnClickListener {

            RegisterAndVerifyEmailAddress()
        }

        btn_sign_in.setOnClickListener {

            LoginUserAndCheckVerification()
        }

        dialog_verify.btn_cancel_verify_email_dialog.setOnClickListener {

            cancelDialog()
        }
        dialog_verify.btn_verify_email.setOnClickListener {

            verifyEmailAddress()
        }
        dialog_forget.btn_cancel_recover_password_dialog.setOnClickListener {

            cancelDialogForgetPassword()
        }

        tv_forgot_password.setOnClickListener {

            recoverPassword()
        }
        iv_profile_set.setOnClickListener {
            setUserProfilePicture()
        }
    }
}
