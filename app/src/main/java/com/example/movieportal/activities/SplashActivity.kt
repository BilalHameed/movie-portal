package com.example.movieportal.activities

import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.example.movieportal.R
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    lateinit var backgroundAnimation:AnimationDrawable
    lateinit var logoAnimation:AnimationDrawable
    lateinit var fadeOut:Animation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        backgroundAnimation = my_layout.background as AnimationDrawable
        logoAnimation = iv_logo.background as AnimationDrawable


        fadeOut = AnimationUtils.loadAnimation(this,R.anim.fade_out)

        with(backgroundAnimation)
        {
            setEnterFadeDuration(800)
            setExitFadeDuration(800)
            isOneShot = true
            start()
        }


        with(logoAnimation)
        {
            setEnterFadeDuration(800)
            setExitFadeDuration(800)
            isOneShot = true
            start()
        }

        with(fadeOut)
        {
            fillAfter = true
            splashing_laoyout.animation = this
            start()
        }

        performTask {


            this.startActivity(Intent(this,SignInSignOutActivity::class.java))
            overridePendingTransition(R.anim.abc_fade_in,R.anim.abc_fade_out)
            finish()
        }

    }

    fun performTask(sec:Long = 2,task:()->Unit)
    {
        Handler().postDelayed({

            task()
        },sec*1000)
    }
}
