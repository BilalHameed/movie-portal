package com.example.movieportal.base

import android.content.Context
import android.content.Intent
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Gravity
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.movieportal.R
import com.example.movieportal.dialogs.ForgetPasswordDialog
import com.example.movieportal.dialogs.LoadingDialog
import com.example.movieportal.dialogs.VerifyEmailDialog
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage

abstract class BaseActivity:AppCompatActivity() {

    lateinit var auth: FirebaseAuth
    lateinit var fbDatabase:FirebaseDatabase
    lateinit var fbStorage:FirebaseStorage

    lateinit var dialog_loading:LoadingDialog
    lateinit var dialog_verify:VerifyEmailDialog
    lateinit var dialog_forget:ForgetPasswordDialog

    abstract fun initialize()


    fun activityChange(target:Class<*>)
    {
        startActivity(Intent(applicationContext,target))
        overridePendingTransition(R.anim.right_to_left,R.anim.left_to_right)
    }

    fun activityChangeWithFinish(target:Class<*>)
    {
        activityChange(target)
        finish()
    }

    fun myThread(seconds:Long = 1,task:()->Unit)
    {
        Handler().postDelayed({task()},seconds * 1000)
    }

    fun View.setAnimationOnAnyView(resourceId:Int): Animation
    {
        val animationUtil = AnimationUtils.loadAnimation(applicationContext,resourceId)
        this.startAnimation(animationUtil)
        return animationUtil
    }

    fun Context.showToast(msg:String, gravity:Int = Gravity.BOTTOM, x:Int = 0, y:Int = 0)
    {
        val toast = Toast.makeText(applicationContext,msg, Toast.LENGTH_SHORT)

        val xAxis:Int = if (x==0) toast.xOffset else x
        val yAxis:Int = if (y==0) toast.yOffset else y
        toast.setGravity(gravity,xAxis,yAxis)
        toast.show()

    }

    fun setToolBar(toolb:Int,title:String,icon:Int)
    {
        val tb = findViewById<Toolbar>(toolb)
        setSupportActionBar(tb)
        supportActionBar?.title = title
        supportActionBar?.setIcon(icon)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    //Extension functions
    fun EditText.clear()
    {
        this.setText("")
    }

    fun TextView.clear()
    {
        this.text = ""
    }

    fun View.hide()
    {
        visibility = View.GONE
    }


}