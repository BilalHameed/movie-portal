package com.example.movieportal.dialogs

import android.app.Activity
import android.app.Dialog
import android.graphics.drawable.ColorDrawable
import android.support.v4.content.ContextCompat
import android.util.DisplayMetrics
import android.view.Gravity
import com.example.movieportal.R
import kotlinx.android.synthetic.main.loading_dialog.*
import kotlinx.android.synthetic.main.verify_email_dialog.*


class VerifyEmailDialog(con:Activity):Dialog(con)
{
    init {

        setContentView(R.layout.verify_email_dialog)
        val display = DisplayMetrics()
        window?.windowManager?.defaultDisplay?.getMetrics(display)
        window.attributes.windowAnimations = R.anim.abc_popup_enter
        window!!.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(context,android.R.color.transparent)))
        dialog_verify_email.gravity = Gravity.CENTER
        dialog_verify_email.layoutParams.width = display.widthPixels/100 * 80

    }
}