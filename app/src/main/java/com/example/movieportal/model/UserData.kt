package com.example.movieportal.model

import android.net.Uri

class UserData {
    var userName:String = ""
    var userId:String = ""
    var userEmail:String = ""
    var userImage:String=""
}